package com.example.useretrofittocallapi.api_ghtk.Controller;

import com.example.useretrofittocallapi.api_ghtk.BuildConfig1;
import com.example.useretrofittocallapi.api_ghtk.model.User;

import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.HeaderMap;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

public interface RequestAPI {
    @Headers({
            "isMobileApp: 1",
            "appVersion: " + BuildConfig1.VERSION_CODE,
            "Content-Type: application/x-www-form-urlencoded"
    })
    @POST
    Call<ResponseBody> doPostWithCookie(
            @Url String url,
            @Header("Cookie") String cookie,
            @Body RequestBody body
    );
    @Headers({
            "isMobileApp: 1",
            "appVersion: " + BuildConfig1.VERSION_CODE,
            "Content-Type: application/x-www-form-urlencoded"
    })
    @POST
    Call<User> doPostWithoutCookie(
            @Url String url,
            @Body RequestBody body
    );

    @Headers({
            "isMobileApp: 1",
            "appVersion: " + BuildConfig1.VERSION_CODE,
            "appType: COD"
    })
    @POST
    Call<ResponseBody> doPostWithCookie(
            @Url String url,
            @Header("Cookie") String cookie,
            @Header("device_id") String device_id,
            @Header("macAddr") String macAddr,
            @Header("wifi_mac") String wifi_mac,
            @Header("wifi_name") String wifi_name,
            @Body RequestBody body
    );


    @DELETE
    @Headers({
            "isMobileApp: 1",
            "appVersion: " + BuildConfig1.VERSION_CODE,
            "appType: COD"
    })
    Call<ResponseBody> doDeleteWithCookie(
            @Url String url,
            @Header("Cookie") String cookie,
            @Header("device_id") String device_id,
            @Header("macAddr") String macAddr,
            @Header("wifi_mac") String wifi_mac,
            @Header("wifi_name") String wifi_name
    );

    @Headers({
            "isMobileApp: 1",
            "appVersion: " + BuildConfig1.VERSION_CODE,
            "appType: COD"
    })
    @POST
    Call<ResponseBody> doPostWithCookieV2(
            @Url String url,
            @Header("token") String cookie,
            @Header("device_id") String device_id,
            @Header("macAddr") String macAddr,
            @Header("wifi_mac") String wifi_mac,
            @Header("wifi_name") String wifi_name,
            @Body RequestBody body
    );

    @POST
    Call<ResponseBody> doPostDeviceWithCookieV2(
            @Url String url,
            @Header("x-access-token") String cookie,
            @Header("Content-Type") String content,
            @Body RequestBody body
    );

    @Headers({
            "isMobileApp: 1",
            "appVersion: " + BuildConfig1.VERSION_CODE,
            "appType: COD"
    })
    @GET
    Call<ResponseBody> doGetWithoutCookie(
            @Url String url
    );

    @Headers({
            "isMobileApp: 1",
            "appVersion: " + BuildConfig1.VERSION_CODE,
            "Content-Type: application/x-www-form-urlencoded"
    })

    @GET
    Call<ResponseBody> doGetWithId(
            @Url String id,
            @Header("Cookie") String cookie
    );

    @Headers({
            "isMobileApp: 1",
            "appVersion: " + BuildConfig1.VERSION_CODE,
            "appType: COD"
    })
    @GET
    Call<ResponseBody> doGetWithCookie(
            @Url String url,
            @Header("Cookie") String cookie,
            @Header("device_id") String device_id,
            @Header("macAddr") String macAddr,
            @Header("wifi_mac") String wifi_mac,
            @Header("wifi_name") String wifi_name
    );

    @Headers({
            "isMobileApp: 1",
            "appVersion: " + BuildConfig1.VERSION_CODE,
            "appType: COD"
    })
    @GET
    Call<ResponseBody> doGetWithCookieV2(
            @Url String url,
            @Header("token") String cookie,
            @Header("device_id") String device_id,
            @Header("macAddr") String macAddr,
            @Header("wifi_mac") String wifi_mac,
            @Header("wifi_name") String wifi_name
    );

    @Headers({
            "isMobileApp: 1",
            "appVersion: " + BuildConfig1.VERSION_CODE,
            "appType: COD"
    })
    @Multipart
    @POST
    Call<ResponseBody> doUploadFileNeedCookie(
            @Url String url,
            @Header("Cookie") String cookie,
            @Part MultipartBody.Part file,
            @PartMap Map<String, RequestBody> params
    );


    @Headers({"isMobileApp: 1", "appVersion: " + BuildConfig1.VERSION_CODE})
    @Multipart
    @POST
    Call<ResponseBody> doUploadMultiFileNeedCookie(

            @Url String url,
            @Header("Cookie") String cookie,
            @Part List<MultipartBody.Part> files,
            @PartMap Map<String, RequestBody> params
    );

    @Headers({"isMobileApp: 1", "appVersion: " + BuildConfig1.VERSION_CODE})
    @Multipart
    @POST
    Call<ResponseBody> doUploadMultiFileNeedCookieV2(

            @Url String url,
            @Header("token") String cookie,
            @Part List<MultipartBody.Part> files,
            @PartMap Map<String, RequestBody> params
    );

    @GET("apps/ghtk_voip.apk")
    @Streaming
    Call<ResponseBody> downloadGHTKVoipFile();

    @GET("apps/ghtk_phone.apk")
    @Streaming
    Call<ResponseBody> downloadGHTKPhoneFile();

    @GET
    Call<ResponseBody> doGet(
            @HeaderMap Map<String, String> headers,
            @Url String url
    );

    @DELETE
    Call<ResponseBody> doDelete(
            @HeaderMap Map<String, String> headers,
            @Url String url
    );

    @POST
    Call<ResponseBody> doPost(
            @HeaderMap Map<String, String> headers,
            @Url String url,
            @Body RequestBody body
    );

    @Multipart
    @POST
    Call<ResponseBody> doPostMultiFile(
            @HeaderMap Map<String, String> headers,
            @Url String url,
            @Part List<MultipartBody.Part> files,
            @PartMap Map<String, RequestBody> params
    );

    @GET("apk/ghtk_phone_v2.apk")
    @Streaming
    Call<ResponseBody> downloadGHTKPhoneFileV2();
}
