package com.example.useretrofittocallapi.employee_api.controller;

import com.example.useretrofittocallapi.ControllerCallBack;
import com.example.useretrofittocallapi.employee_api.model.Employee;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.ObservableSource;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.functions.Supplier;
import io.reactivex.rxjava3.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class EmployeeController {
    private Retrofit retrofit = EmployeeRequestService.getInstance();
    private static final String allEmployee = "/api/v1/employees";

    public void getAllEmployee(ControllerCallBack callBack) {
        getRequestApi().doGetAllEmployee(allEmployee).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200 && response.isSuccessful()) {
                    if (response.body() != null) {
                        Observable.just(response.body()).subscribeOn(Schedulers.io()).flatMap(responseBody -> {
                            try {
                                String value = response.body().string();
                                JSONObject object = new JSONObject(value);
                                List<Employee> employees;
                                employees = parseJsonToGetEmployees(object);
                                return Observable.defer((Supplier<ObservableSource<?>>) () -> Observable.just(employees));
                            } catch (Exception e) {
                                return Observable.error(e);
                            }
                        }).observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<Object>() {
                            @Override
                            public void onSubscribe(@NonNull Disposable d) {

                            }

                            @Override
                            public void onNext(@NonNull Object o) {
                                callBack.onFinish(o);
                            }

                            @Override
                            public void onError(@NonNull Throwable e) {
                                callBack.onFailure(e);
                            }

                            @Override
                            public void onComplete() {

                            }
                        });
                    } else {
                        callBack.onFailure("");
                    }
                } else {
                    callBack.onFailure("");
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                callBack.onFailure("");
            }
        });
    }

    private List<Employee> parseJsonToGetEmployees(JSONObject jsonObject) {
        ArrayList<Employee> employees = new ArrayList<>();
        if (jsonObject != null) {
            if (!jsonObject.isNull("status") && jsonObject.has("status")) {
                try {
                    String rs = jsonObject.getString("status");
                    if (rs.equals("success")) {
                        if (jsonObject.has("data") && !jsonObject.isNull("data")) {
                            //dung gson
                            employees = new Gson().fromJson(jsonObject.get("data").toString(), new TypeToken<List<Employee>>() {
                            }.getType());
                            return employees;
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return employees;
    }

    private ArrayList<Employee> getEmployeeFromJson(JSONObject jsonObject) {
        // dùng rxjava, sửa lai chút dùng flatmap, thử dùng filter

        Observable.just(jsonObject).subscribeOn(Schedulers.newThread()).map(new Function<JSONObject, ArrayList<Employee>>() {
            @Override
            public ArrayList<Employee> apply(JSONObject jsonObject) throws Throwable {
                ArrayList<Employee> employees = new ArrayList<>();
                if (jsonObject != null) {
                    if (!jsonObject.isNull("status") && jsonObject.has("status")) {
                        try {
                            String rs = jsonObject.getString("status");
                            if (rs.equals("success")) {
                                if (jsonObject.has("data") && !jsonObject.isNull("data")) {
                                    //dung gson
                                    employees = new Gson().fromJson(jsonObject.get("data").toString(), new TypeToken<List<Employee>>() {
                                    }.getType());
                                    return employees;
                                    //parse tay
//                            JSONArray array = jsonObject.getJSONArray("data");
//                            for (int i = 0; i < array.length(); i++) {
//                                JSONObject object = array.getJSONObject(i);
//                                Employee employee = new Employee(object);
//                                employees.add(employee);
//                            }
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
                return employees;
            }
        }).observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<ArrayList<Employee>>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull ArrayList<Employee> employees) {

            }

            @Override
            public void onError(@NonNull Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });


        // code như bao đời nay vẫn code
        ArrayList<Employee> employees = new ArrayList<>();
        if (jsonObject != null) {
            if (!jsonObject.isNull("status") && jsonObject.has("status")) {
                try {
                    String rs = jsonObject.getString("status");
                    if (rs.equals("success")) {
                        if (jsonObject.has("data") && !jsonObject.isNull("data")) {
                            //dung gson
                            employees = new Gson().fromJson(jsonObject.get("data").toString(), new TypeToken<List<Employee>>() {
                            }.getType());
                            return employees;
                            //parse tay
//                            JSONArray array = jsonObject.getJSONArray("data");
//                            for (int i = 0; i < array.length(); i++) {
//                                JSONObject object = array.getJSONObject(i);
//                                Employee employee = new Employee(object);
//                                employees.add(employee);
//                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return employees;
    }

    private EmployeeAPI getRequestApi() {
        return retrofit.create(EmployeeAPI.class);
    }

    public void getAllEmployeeUseGson(ControllerCallBack error) {
        getRequestApi().doGetAllEmployeeUseGson(allEmployee).enqueue(new Callback<List<Employee>>() {
            @Override
            public void onResponse(Call<List<Employee>> call, Response<List<Employee>> response) {
                if (response.body() != null) {
                    error.onFinish(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<Employee>> call, Throwable t) {
                error.onFailure("");
            }
        });
    }
}
