package com.example.useretrofittocallapi.employee_api.controller;

import com.example.useretrofittocallapi.api_ghtk.BuildConfig1;
import com.example.useretrofittocallapi.employee_api.model.Employee;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Url;

public interface EmployeeAPI {
    @Headers({
            "isMobileApp: 1",
            "appVersion: " + BuildConfig1.VERSION_CODE,
            "Content-Type: application/x-www-form-urlencoded"
    })
    @GET
    Call<ResponseBody> doGetAllEmployee(
            @Url String url
    );

    @Headers({
            "isMobileApp: 1",
            "appVersion: " + BuildConfig1.VERSION_CODE,
            "Content-Type: application/x-www-form-urlencoded"
    })
    @GET
    Call<List<Employee>> doGetAllEmployeeUseGson(
            @Url String url
    );
}
