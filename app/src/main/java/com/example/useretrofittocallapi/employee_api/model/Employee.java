package com.example.useretrofittocallapi.employee_api.model;

import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

public class Employee {
    @SerializedName("id")
    private int id;
    @SerializedName("employee_name")
    private String name;
    @SerializedName("employee_age")
    private int age;
    @SerializedName("profile_image")
    private String profileImage;
    @SerializedName("employee_salary")
    private int salary;

    public Employee(int id, String name, int age, String profileImage, int salary) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.profileImage = profileImage;
        this.salary = salary;
    }

    public Employee(JSONObject object) {
        try {
            if (object.has("id") && !object.isNull("id")) {
                this.id = object.getInt("id");
            }
            if (object.has("employee_name") && !object.isNull("employee_name")) {
                this.name = object.getString("employee_name");
            }
            if (object.has("employee_salary") && !object.isNull("employee_salary")) {
                this.salary = object.getInt("employee_salary");
            }
            if (object.has("employee_age") && !object.isNull("employee_age")) {
                this.age = object.getInt("employee_age");
            }
            if (object.has("profile_image") && !object.isNull("profile_image")) {
                this.profileImage = object.getString("profile_image");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public int getSalary() {
        return salary;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }
}
