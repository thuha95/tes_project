package com.example.useretrofittocallapi.testRxJava;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.useretrofittocallapi.R;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.ObservableEmitter;
import io.reactivex.rxjava3.core.ObservableOnSubscribe;
import io.reactivex.rxjava3.core.ObservableSource;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.functions.Predicate;
import io.reactivex.rxjava3.functions.Supplier;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class RxJavaFragment extends Fragment {
    private Button button;
    private TextView responseText;

    @Nullable
    @Override
    public View onCreateView(@androidx.annotation.NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.frg_rx_java, container, false);
        initView(v);
        return v;
    }

    private void initView(View view) {
        button = view.findViewById(R.id.bt_find);
        responseText = view.findViewById(R.id.tv_rs);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                doCreate();
//                doThreadConcept();
                testIntervalObservable();
//                testCreateObservalbe();
                testOperatorFilter();
            }
        });


    }

    private void testOperatorFilter() {
        // filer lọc ra số item thỏa mãn đk, take (2) định nghĩa số item được trả về.
        Observable.just(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
                .filter(integer -> integer % 2 == 0).take(2).toList().subscribe(integers -> {
            Log.i("TestFilter", integers.toString());
        });

        // khác nhau giữa hai kiểu truyền vào
        Integer[] integers = new Integer[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        Observable.just(integers).filter(new Predicate<Integer[]>() {
            @Override
            public boolean test(Integer[] integers) throws Throwable {
                return false;
            }
        });

        // --> chuyển sang dùng fromIterable

    }

    Movie movie = new Movie("Captain America", " Civil War");

    private void doThreadConcept() {
//        getStringObservable().observeOn(Schedulers.newThread()).map(integer -> {
//            Log.i("Observable 2", Thread.currentThread().getName() );
//            return String.valueOf(integer);
//        }).subscribeOn(Schedulers.newThread()).subscribe(new Observer<String>() {
//            @Override
//            public void onSubscribe(@NonNull Disposable d) {
//
//            }
//
//            @Override
//            public void onNext(@NonNull String s) {
//                Log.i("Observable 3", Thread.currentThread().getName() );
//                Log.i("Observable onNext", s);
//            }
//
//            @Override
//            public void onError(@NonNull Throwable e) {
//
//            }
//
//            @Override
//            public void onComplete() {
//
//            }
//        });

        getStringObservable().map(new Function<String, Integer>() {
            @Override
            public Integer apply(String s) throws Throwable {
                return Integer.parseInt(s);
            }
        }).subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<Integer>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull Integer integer) {
                Toast.makeText(getContext(), integer + " hello", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(@NonNull Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    private Observable<Integer> getANumberObservable() {
        return Observable.defer(new Supplier<ObservableSource<? extends Integer>>() {
            @Override
            public ObservableSource<? extends Integer> get() throws Throwable {
                Log.i("Observable 1", Thread.currentThread().getName());
                return Observable.just(1);
            }
        });
    }

    private Observable<String> getStringObservable() {
        return Observable.defer(() -> {
            Log.i("Observable thread: ", Thread.currentThread().getName());
            return Observable.just("1");
        });

//        return Observable.just(doSomething());
    }

    private String doSomething() {
        // hàm just và from nhật giá trị ngay từ khi khởi tạo --> không xử lí logic nặng trong này. Thay vào đó thì dùng defer. nhận giá trị khi hàm subscribe được gọi.
        Log.i("Observable thread: ", Thread.currentThread().getName());
        return "A";
    }

    private void doCreate() {
        Integer[] integers = {1, 2, 3};

        //just
        Observable.just(integers).subscribe(new Observer<Integer[]>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            public void onNext(Integer[] integers) {
                Log.i("onNext", Arrays.toString(integers));
            }

            @Override
            public void onError(@NonNull Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });

        // from replace by fromArray
        Observable.fromArray(integers).subscribe(new Observer<Integer>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            public void onNext(Integer integer) {
                Log.i("onNext", String.valueOf(integer));
            }

            @Override
            public void onError(@NonNull Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });

        //defer, update date
        Observable<Movie> movieObservable = Observable.defer(new Supplier<ObservableSource<? extends Movie>>() {
            @Override
            public ObservableSource<? extends Movie> get() throws Throwable {
                return Observable.just(movie);
            }
        });
        movie = new Movie("Batman v Superman", " Dawn of Justice");
        movieObservable.subscribe(new Observer<Movie>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            public void onNext(Movie movie) {
                Log.i("onNext", movie.name);
            }

            @Override
            public void onError(@NonNull Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });


    }

    private void testCreateObservalbe() {
        //create
//        Observable.create(new ObservableOnSubscribe<Integer>() {
//            @Override
//            public void subscribe(@NonNull ObservableEmitter<Integer> emitter) throws Throwable {
//                emitter.onNext(1);
//                emitter.onNext(2);
//                emitter.onNext(3);
//                emitter.onNext(4);
//                emitter.onComplete();
//            }
//        }).subscribe(new Consumer<Integer>() {
//            @Override
//            public void accept(Integer integer) throws Throwable {
//                Log.i("onNext", "" + integer);
//            }
//        });

        Observable.create(new ObservableOnSubscribe<Integer>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<Integer> emitter) throws Throwable {
                Log.i("Observable thread", Thread.currentThread().getName());
                emitter.onNext(1);
                emitter.onComplete();
            }
        }).observeOn(Schedulers.newThread()).subscribe(new Observer<Integer>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull Integer integer) {
                Log.i("Observable thread", Thread.currentThread().getName() + integer);
            }

            @Override
            public void onError(@NonNull Throwable e) {

            }

            @Override
            public void onComplete() {
                Log.i("Observable thread", Thread.currentThread().getName() + " complete");
            }
        });

    }

    private void testIntervalObservable() {
        // interval
        Observable.interval(1, TimeUnit.SECONDS).observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<Long>() {
            Disposable disposable;

            @Override
            public void onSubscribe(@NonNull Disposable d) {
                disposable = d;
            }

            @Override
            public void onNext(@NonNull Long aLong) {
                updateUI(aLong);
                // tự chạy trên một thread khác.
                // call api ở đây, được dữ liệu thì update UI bằng cách runOnUIthread.....
                Log.i("Observable thread", Thread.currentThread().getName() + aLong);
                if (aLong > 11) {
                    disposable.dispose();
                }
            }

            @Override
            public void onError(@NonNull Throwable e) {

            }

            @Override
            public void onComplete() {
                // không nhảy vào onComplete
                Log.i("Observable 1 thread", Thread.currentThread().getName());
            }
        });

    }

    private void updateUI(Long aLong) {
        if (aLong > 10) {
            responseText.setText("Time up");
        } else {
            responseText.setText(aLong + "");
        }
    }

}
